import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LikeDislikeComponent } from './components/like-dislike/like-dislike.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';

const routes: Routes = [
  
  {
    path: "like",
    component: LikeDislikeComponent
  },
  {
    path:"todo",
    component:TodoListComponent
  }
 


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
