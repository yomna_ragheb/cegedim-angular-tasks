import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-like-dislike',
  templateUrl: './like-dislike.component.html',
  styleUrls: ['./like-dislike.component.css']
})
export class LikeDislikeComponent implements OnInit {
  likes: number;
  dislikes: number;
  isliked: boolean
  isDisLiked: boolean
  constructor() { }

  ngOnInit(): void {
    this.likes = 100;
    this.dislikes = 25;
this.isliked=false
 console.log(this.isliked)


  }

  toggleLike() {
    if (!this.isliked) {
      console.log(this.isliked)
    this.isliked = true;
    this.likes += 1
    console.log(this.isliked)

    }
    else {
      this.likes -= 1
      this.isliked=false
    }
  }

  toggleDislike() {
    if (!this.isDisLiked) {
      console.log(this.isDisLiked)
    this.isDisLiked = true;
    this.dislikes += 1
    console.log(this.isDisLiked)

    }
    else {
      this.dislikes -= 1
      this.isDisLiked=false
    }
  }
}

